const GIFS = [
  '1.gif',
  '2.gif',
  '3.gif',
  '4.gif',
  '5.gif',
  '6.gif',
  '7.gif',
  '8.gif',
  '1.gif',
  '2.gif',
  '3.gif',
  '4.gif',
  '5.gif',
  '6.gif',
  '7.gif',
  '8.gif',
];
function shuffle(array) {
  let counter = array.length;

  while (counter > 0) {
    let index = Math.floor(Math.random() * counter);

    counter--;

    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

let shuffleGifs = shuffle(GIFS);

function createDivsForGifs(gifArray) {
  for (let gif of gifArray) {
    const newDiv = document.createElement('div');

    newDiv.classList.add(gif);

    const newImg = document.createElement('img');

    newImg.setAttribute('src', `./gif/${gif}`);

    newDiv.append(newImg);

    newDiv.addEventListener('click', handleCardClick);

    gameContainer.append(newDiv);
  }
}

const gameScreen = document.querySelector('#game-screen');
const gameContainer = document.querySelector('#game-container');
const startButton = document.querySelector('#start-btn');
const startScreen = document.querySelector('#start-screen');
const restartButton = document.querySelector('#restart-btn');
const overlay = document.querySelector('#overlay');
const scoreValues = document.querySelectorAll('.score');
const bestscoreValues = document.querySelectorAll('.best-score');
const homeButton = document.querySelector('#home-btn');

let showCards = [];
let cardsTurned = 0;
let guessCount = 0;
let bestScore = localStorage.getItem('bestscore');

if (bestScore) {
  for (let bestscore of bestscoreValues) {
    bestscore.innerText = bestScore;
  }
}

function handleCardClick(event) {
  // console.log(this.children[0].style.display);
  if (showCards.length < 2 && this.children[0].style.display !== 'block') {
    this.children[0].style.display = 'block';

    guessCount += 1;

    for (let score of scoreValues) {
      score.innerText = guessCount;
    }

    if (showCards.length === 0) {
      showCards.push(this);
    } else if (showCards.length === 1 && showCards[0] !== this) {
      showCards.push(this);

      if (showCards[0].children[0].src === showCards[1].children[0].src) {
        cardsTurned += 2;
        console.log(cardsTurned);
        showCards = [];

        // GAME OVER
        if (cardsTurned === GIFS.length) {
          if (bestScore === null) {
            console.log('gameover');
            localStorage.setItem('bestscore', `${parseInt(guessCount)}`);
            for (let bestscore of bestscoreValues) {
              bestscore.innerText = localStorage.getItem('bestscore');
            }
          } else if (parseInt(guessCount) < parseInt(bestScore)) {
            localStorage.setItem('bestscore', `${parseInt(guessCount)}`);
            for (let bestscore of bestscoreValues) {
              bestscore.innerText = localStorage.getItem('bestscore');
            }
          }
          overlay.style.display = 'block';
        }
      } else {
        function turnBack() {
          showCards[0].children[0].style.display = 'none';
          showCards[1].children[0].style.display = 'none';

          showCards = [];
        }
        setTimeout(turnBack, 1000);
      }
    }
  }
}

startButton.addEventListener('click', () => {
  showCards = [];
  guessCount = 0;
  cardsTurned = 0;
  gameContainer.innerHTML = '';
  shuffle(GIFS);
  gameScreen.classList.toggle('hide');
  startScreen.style.display = 'none';
  gameContainer.style.height = '100%';

  createDivsForGifs(shuffleGifs);
});

restartButton.addEventListener('click', () => {
  showCards = [];
  guessCount = 0;
  cardsTurned = 0;
  gameContainer.innerHTML = '';
  guessCount = 0;
  shuffle(GIFS);
  for (let score of scoreValues) {
    score.innerText = guessCount;
  }

  createDivsForGifs(shuffleGifs);
  overlay.style.display = 'none';
});

homeButton.addEventListener('click', () => {
  showCards = [];
  guessCount = 0;
  cardsTurned = 0;
  gameContainer.innerHTML = '';
  guessCount = 0;
  for (let score of scoreValues) {
    score.innerText = guessCount;
  }
  shuffle(GIFS);
  gameScreen.classList.toggle('hide');
  startScreen.style.display = 'block';
});
